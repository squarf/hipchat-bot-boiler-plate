// New Relic
//require('newrelic');

var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');

if (process.env.localBaseUrl) {
    pkg.production.localBaseUrl = process.env.localBaseUrl;   
}
else{
    console.log("You are missing the localBaseURL configuration variable");
}
var app = ack(pkg);

// Ladder Bot dependencies
var bunyan = require('bunyan');
var pgp = require('pg-promise')(/*options*/);

// Ladder Bot Services
var SampleService = require('./services/sampleService.js');
var RouterService = require('./services/routerService.js');


// Handlers
var SampleHandler = require('./handlers/ladder/sampleHandler.js');
var HelpHandler = require('./handlers/helpHandler.js');
var ErrorHandler = require('./handlers/errorHandler.js');

//Command Parsers
var SampleParser = require('./parsers/sampleParser.js');

// Handlebars
require('./templates/ladder/helpers.js');

// Setup logging
var log = bunyan.createLogger({
    name: 'yourbot'
});


log.info("Starting up.");
log.info("Setting up koa app and hipchat permissions.");

// Setup the app, request needed permissions
var addon = app.addon()
    .hipchat()
    .allowRoom(true)
    .scopes('send_notification');

// Setup the key used by hipchat
if (process.env.BOT_KEY) {
  addon.key(process.env.BOT_KEY);
}

if (process.env.APP_NAME) {
  addon.name(process.env.APP_NAME);
}

if (process.env.POSTGRES_URL) {
  var conString = (process.env.POSTGRES_URL);
}

//if (process.env.APP_DNAME) {
//  addon.displayname(process.env.APP_DNAME);
//}

log.info("Registering the handlers.");

// Instantiate singleton services

var sampleService = new SampleService(log, conString);

//instantiating parsers
var sampleParser = new SampleParser(log);

// Register the handlers
var botRouter = new RouterService(log, conString);
botRouter.add(new SampleHandler(sampleService, sampleParser, log));
botRouter.add(new HelpHandler(botRouter, log));


// Define the webhooks
log.info("Defining the webhooks to be used by hipchat.");

addon.onWebhook('install', function * () {
    yield this.roomClient.sendNotification('Hello I am the new bot!');
});

addon.onWebhook('uninstall', function * () {
    yield this.roomClient.sendNotification('WHY U UNINSTALL?');
});

// Helper notes:
// This is where we register what messages hipchat will send to us
addon.webhook('room_message', /^\/bot /, function * () {
    try{
        // We are getting a new message that starts with /ladder, give it to the ladder router
        // The ladder router will pass it to the correct handler based on the next word /ladder {command} ex. "/ladder join" would go to the joinHandler
        //		console.log("Hi, "+ this.sender.name);
		var room = this.room;
		yield this.roomClient.sendNotification(yield botRouter.route(this.content, this.sender, room));
    }catch (e){
        var errorHandler = new ErrorHandler();
        yield this.roomClient.sendNotification(yield errorHandler.route(this.sender.name));
    }

});

log.info("Starting the koa app.");

app.listen();

log.info("Start up successfull!");