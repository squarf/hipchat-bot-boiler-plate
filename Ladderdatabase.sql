create sequence challengeID start 1;

create table participants(
hipchatname varchar(255),
hipmentionname varchar(255),
hipchatid varchar(255) NOT NULL,
roomid VARCHAR(255),
acceptingChallenges VARCHAR(255),
rank INT
);

create table challenges(
challengerHipID varchar(255),
challengeeHipID varchar(255),
roomid varchar(255),
lichessurl varchar(255),
challengeID INT
)

insert into participants(hipchatname, hipmentionname, hipchatid, roomid, rank) 
values ('testName', '@test', '1', '2095865', 1);

insert into challenges(challengerHipID, challengeeHipID, roomid) 
values ('2297557', '1819924');