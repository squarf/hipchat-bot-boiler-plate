var handlebars = require("handlebars");

handlebars.registerHelper('listOneIndent', function(items, options) {
  var out = "";
  if (items){
	for(var i=0, l=items.length; i<l; i++) {
		out = out + "<li>  " + options.fn(items[i]) + "</li>";
	}
  }
  return out;
});

handlebars.registerHelper('listTwoIndent', function(items, options) {
  var out = "<ul>";
  if (items){
	for(var i=0, l=items.length; i<l; i++) {
		out = out + "<li>" + options.fn(items[i]) + "</li>";
	}
  }
  return out + "</ul>";
});


handlebars.registerHelper('if', function(conditional, options) {
  if(conditional) {
    return options.fn(this);
  }
});