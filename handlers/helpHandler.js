var fs = require('fs');
var handlebars = require("handlebars");
//

function HelpHandler(handlerService, log) {
    this.pageBuilder = handlebars.compile(fs.readFileSync("./templates/ladder/help.hbs", "utf8"));
    this.handles = "help";
    this.description = "Lists all of the available commands.";
    this.handlerService = handlerService;
    this.log = log;
};

HelpHandler.prototype.handle = function * (command, sender, room) {
    var source = {};
    var commands = [];
	var subcommands = {};

    if (this.handlerService) {
        var handlers = this.handlerService.getHandlers();
    }

    for (var key in handlers) {
        var handlerViewModel = {};
        //TODO change /bot to what your actual matching command is
        handlerViewModel['command'] = '/bot ' + handlers[key].handles;
        handlerViewModel['description'] = handlers[key].description;
		if(handlers[key].subcommands){
			handlerViewModel['subcommands'] = handlers[key].subcommands.commands;
		}
		commands.push(handlerViewModel);
		}
    var source = {
        commands: commands
    };
    return this.pageBuilder(source);
}

module.exports = HelpHandler;
