var fs = require('fs');
var handlebars = require("handlebars");

function SampleHandler(sampleService, sampleParser, log) {
    this.pageBuilder = handlebars.compile(fs.readFileSync("./templates/ladder/sample.hbs", "utf8"));
    this.handles = "command"; //this is the string that gets matched after the initial command /bot command subcommand
    this.description = "will pair you against the person with the fewest active games";
    this.subcommands = { commands: [
							{name: "subcommand", description: "for commands longer than /bot firstcommand."}
						]
						}
	this.log = log;
    this.sampleService = sampleService;
    this.sampleParser = sampleParser;
};

SampleHandler.prototype.handle = function* (words, sender, room) {
    var command = yield this.sampleParser.parse(words);//use the parser if you find yourself parsing difficult hings.
    //you can use yield this.sampleService.function(argument1, argument2) if you want to use the service to handle things outside of this object.

    return "You return text and the bot says it!";
    
}
	
module.exports = SampleHandler;