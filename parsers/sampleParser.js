var fs = require('fs');
var handlebars = require("handlebars");

function SampleParser(log) {
	this.log = log;
};

SampleParser.prototype.parse = function* (words, sampleService, roomid) {
    
    var parsedCommand = {
        subCommand : "",
        errorCode : null
    };
 
    //quick check for length
    if (words.length > 6) {
        parsedCommand.errorCode = "long";
    }
    //an example of the parsing that can be done.
	return parsedCommand;
}



module.exports = SampleParser;