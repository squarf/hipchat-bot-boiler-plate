HipchatBot Boiler Plate
=======

Created by: 
Nathan Weyenberg
with significant help from Jared Prather

## What is the hipchatbot Boiler Plate?
- This project is meant get someone started on making a hipchat bot using node.js

## Current Commands:

/bot sample  sample command
/bot help    Lists all of the available commands.

## The details
- Node.js
- Koa.js
- Q.js
- pg-promise.js
- Handlebars.js


##Want to contribute?
Fork the repo and create pull requests!

##To get started developing your Hipchat Bot! 

-----------------------------
Preparatory Steps: 
-----------------------------
1. Install a git tool. (one can be found here: http://www.git-scm.com/downloads , make sure to install git bash for the easiest/quickest way to get this working)
2. Install node.js https://nodejs.org/en/
3. Create an account in Heroku -- https://heroku.com/ (Heroku will provide free hosting for you! at least for the Dev versions at the time of this writting)
4. Install the heroku toolbelt -- https://toolbelt.heroku.com/

-------------------------------
Create your Database (If Needed, feel free to choose a different hosting environment for databases if you wish or skip this step if you don't want to use postgres/any database)
------------------------------
1. create your own postgress database: https://postgres.heroku.com/databases (choosing the dev option is free as of this writting)
2. Note the url of the database to be used later on when you are setting up the bot. You WILL have to modify the connection string at the end to include ?ssl=true in order to connect
3. Create two of these, one for your testing/development environment and one for your production environment

--------------------
Setting up your server for testing/hosting
---------------------
1. Create a new app in Heroku (you should do this process twice, once for test and once for prod).
2. Add the following resources to that app: 
	1. MongoLab	Sandbox	Free	
	2. New Relic APM :: Newrelic	Wayne	Free	
	3. Redis Cloud :: White	30MB	Free
3. create the following environment variables:
	1. localBaseUrl      -- http://whateveryoucalledyourserver.herokuapp.com
	2. POSTGRES_URL      -- should be the full connection url including ?ssl=true at the end! (should be of the form postgres:// ...... ?ssl=true
	3. BOT_KEY           -- should be a unique key for the bot (I used https://www.guidgenerator.com/online-guid-generator.aspx to generate a guid)
	4. APP_NAME		  -- should be a unique bot name, I do (TESTbotname, and botname)

----------------
Suggestions for coding environment
---------------
1. If using Visual Studio, please download https://www.visualstudio.com/en-us/features/node-js-vs.aspx
1. I have included the solution and project files for Visual Studio, feel free to use that or something else to modify the code in!

---------------------------
How to run, deploy and test what you've done!
---------------------------
1. Fork/Clone repository
2. 'npm install' in the git shell in directory you cloned the repository
3. 'git init' initializes the a local git repository in the directory you are in
4. 'git remote add test <your Heroku Test server git url>' 
5. Add the files you want to store in your source repository. MAKE SURE NOT TO ADD node_modules, it causes problems
	-You can add /node_modules/* to .git/info/exclude file 
	- run 'git add *'
6. 'git commit -a -m "your message"' to commit your changes
7. 'git push test master' to push your changes to the heroku server. If you are using a different branch in git the following syntax will be more appropriate: git push test your_branch:master because the heroku server is only configured to rebuild when something is pushed to master. This allows your local repository to remain clean while the test server is.... VERY messy."
8. 'heroku login' to login to heroku via your shell
9. 'heroku logs -t --app yourHerokuServerName' will allow you to see the output of console.log("");  and be able to see runtime errors. This is an invaluable tool for developing and debugging.
10. Go to your hipchat integrations (you might need to be an administrator of your hipchat instance in order to do this)
11. Install an integration using a url: "/http://yourLocalBaseUrl.herokuapp.com/addon/capabilities
12. You should see the installation message if it worked correctly!
13. Interact with your bot: by default the way to interact is by saying /bot anything!

Notes:
I suggest setting up test and prod instances, testing by interacting with the bot as node.js can be difficult to test locally since it is dealing with hipchat messages and outputs.
Create a single room, install the test integration, test it out, when its ready rebase -i your git repo to clean it up and push it to prod which should have a separate integration url.

Enjoy!

